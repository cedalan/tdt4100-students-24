# Øvingstekster

Denne mappen inneholder øvingstekster for TDT4100 - Objektorientert programmering våren 2023. Tabellen under inneholder linker til hver enkelt oppgavetekst og tema for øvingen. Linker vil bli oppdatert underveis.

| Øving                         | Tema                               |
| ----------------------------- | ---------------------------------- |
| [Øving 0](./oving0/README.md) | Introduksjon og oppsett av Java    |
